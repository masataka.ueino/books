from flask_book import db
from datetime import datetime

class Entry(db.Model):
    __tablename__ = 'entries'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text, unique=True)
    description = db.Column(db.Text)
    publisher = db.Column(db.Text)
    publisheddate = db.Column(db.Text)
    identifier10 = db.Column(db.Text)
    identifier13 = db.Column(db.Text)
    thumbnail = db.Column(db.Text)
    previewlink = db.Column(db.Text)
    tanto = db.Column(db.Text)
    hokan = db.Column(db.Text)
    memo = db.Column(db.Text)
    created_at = db.Column(db.DateTime)

    def __init__(self, title=None, description=None, publisher=None, publisheddate=None, identifier10=None, identifier13=None, thumbnail=None, previewlink=None, tanto=None, hokan=None, memo=None):
        self.title = title
        self.description = description
        self.publisher = publisher
        self.publisheddate = publisheddate
        self.identifier10 = identifier10
        self.identifier13 = identifier13
        self.thumbnail = thumbnail
        self.previewlink = previewlink
        self.tanto = tanto
        self.hokan = hokan
        self.memo = memo
        self.created_at = datetime.utcnow()

    def __repr__(self):
        return '<Entry id:{} title:{} description:{} publisher:{} publisheddate:{} identifier10:{} identifier13:{} thumbnail:{} previewlink:{} tanto:{} hokan:{} memo:{}>'.format(self.id, self.title, self.description, self.publisher, self.publisheddate, self.identifier10, self.identifier13, self.thumbnail, self.previewlink, self.tanto, self.hokan, self.memo)
