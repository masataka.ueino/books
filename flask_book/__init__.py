from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_object('flask_book.config')

db = SQLAlchemy(app)

from flask_book.views.entries import entry

app.register_blueprint(entry, url_prefix='/list')

from flask_book.views import views
