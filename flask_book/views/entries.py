from flask import request, redirect, url_for, render_template, flash, session
from flask_book import app
from flask_book import db
from flask_book.models.entries import Entry
from flask_book.views.views import login_required
from flask import Blueprint
from flask_paginate import Pagination, get_page_parameter
import sys
import datetime
import requests
import json
from sqlalchemy import or_

entry = Blueprint('entry', __name__)

@entry.route('/')
@entry.route('/', defaults={'page': 1})
@entry.route('/<int:page>')
def show_entries():
    entries=Entry.query.order_by(Entry.id.desc()).all()
    return render_template('entries/index.html', entries=entries)


@entry.route('/', methods=['POST'])
# @login_required
def search_entries():
    try:
        searchword = request.form['searchword']
    except:
        searchword = ""

    # per_page = 20
    # search = False
    # q = request.args.get('q')
    # if q:
    #     search = True
    # name = request.args.get('name', '')
    # page = request.args.get(get_page_parameter(), type=int, default=1)
    # p = User.query.filter((User.name.like('%' + name + '%')) | (not name)).order_by(User.username.asc()).paginate(page,perpage)
    # entries = Entry.query.filter((Entry.title.like('%' + searchword + '%'))).order_by(Entry.id.desc())
    entries = Entry.query.filter(or_(
        (Entry.title.like('%' + searchword + '%')),
        (Entry.description.like('%' + searchword + '%')),
        (Entry.publisher.like('%' + searchword + '%')),
        (Entry.publisheddate.like('%' + searchword + '%')),
        (Entry.identifier10.like('%' + searchword + '%')),
        (Entry.identifier13.like('%' + searchword + '%')),
        (Entry.tanto.like('%' + searchword + '%')),
        (Entry.hokan.like('%' + searchword + '%')),
        (Entry.memo.like('%' + searchword + '%')),
    )).order_by(Entry.id.desc())
    # return render_template( 'user.html', pagination=p,name=name)
    # pagination = Pagination(page=page, total=3, search=search, record_name='entries')
    # return render_template('entries/index.html', entries=entries, pagination=pagination)
    return render_template('entries/index.html', entries=entries)
    # pagination = Pagination(page=page, total=entries.count(), search=search, record_name='entries', css_framework='foundation', **kwargs)



@entry.route('/alllist')
@login_required
def show_alllist():
    entries = Entry.query.all()
    return render_template('entries/alllist.html', entries=entries)


@entry.route('/entries', methods=['POST'])
@login_required
def add_entry():
    try:
        entry = Entry(
                title=request.form['title'],
                description=request.form['description'],
                publisher=request.form['publisher'],
                publisheddate=request.form['publisheddate'],
                identifier10=request.form['identifier10'],
                identifier13=request.form['identifier13'],
                thumbnail=request.form['thumbnail'],
                previewlink=request.form['previewlink'],
                tanto=request.form['tanto'],
                hokan=request.form['hokan'],
                memo=request.form['memo'],
                )
        db.session.add(entry)
        db.session.commit()
        flash('書籍情報が登録されました', 'alert alert-info')
        return redirect(url_for('entry.show_entries'))
    except:
        flash('エラーが発生しました (同じタイトルの書籍がすでに登録されているかもしれません)', 'alert alert-danger')
        return redirect(url_for('entry.show_entries'))

@entry.route('/entries/get_isbn', methods=['POST'])
@login_required
def get_isbn():
    isbn = request.form['getisbn']
    url = 'https://www.googleapis.com/books/v1/volumes?q=isbn:'
    isbn_path = "./isbn_data/" + isbn + ".json"
    titlevalue = ""
    descriptionvalue = ""
    publishervalue = ""
    publisheddatevalue = ""
    identifier10value = ""
    identifier13value = ""
    thumbnailvalue = ""
    previewlinkvalue = ""
    try:
        with open(isbn_path, 'r') as f:
            data = json.load(f)
            try:
                titlevalue += data['items'][0]['volumeInfo']['title']
                try:
                    descriptionvalue += data['items'][0]['volumeInfo']['description']
                except:
                    # flash('概要を参照できませんでした', 'alert alert-danger')
                    pass
                try:
                    publishervalue += data['items'][0]['volumeInfo']['authors'][0]
                except:
                    # flash('発行者を参照できませんでした', 'alert alert-danger')
                    pass
                try:
                    publisheddatevalue += data['items'][0]['volumeInfo']['publishedDate']
                except:
                    # flash('発行日を参照できませんでした', 'alert alert-danger')
                    pass
                try:
                    if data['items'][0]['volumeInfo']['industryIdentifiers'][0]['identifier'] == isbn:
                        identifier10value += data['items'][0]['volumeInfo']['industryIdentifiers'][1]['identifier']
                    else:
                        identifier10value += data['items'][0]['volumeInfo']['industryIdentifiers'][0]['identifier']
                except:
                    # flash('ISBN-10を参照できませんでした', 'alert alert-danger')
                    pass
                try:
                    if data['items'][0]['volumeInfo']['industryIdentifiers'][1]['identifier'] == isbn:
                        identifier13value += data['items'][0]['volumeInfo']['industryIdentifiers'][1]['identifier']
                    else:
                        identifier13value += data['items'][0]['volumeInfo']['industryIdentifiers'][0]['identifier']
                except:
                    # flash('ISBN-13を参照できませんでした', 'alert alert-danger')
                    pass
                try:
                    thumbnailvalue += data['items'][0]['volumeInfo']['imageLinks']['thumbnail']
                except:
                    # flash('サムネイルを参照できませんでした', 'alert alert-danger')
                    thumbnailvalue += ""
                    # pass
                try:
                    previewlinkvalue += data['items'][0]['volumeInfo']['previewLink']
                except:
                    # flash('プレビューを参照できませんでした', 'alert alert-danger')
                    pass
                flash('書籍情報を参照しました', 'alert alert-info')
            except:
                flash('書籍情報を参照できませんでした', 'alert alert-danger')


    except:
        req_url = url + isbn
        response = requests.get(req_url)
        with open(isbn_path, 'w') as f:
            json.dump(response.json(), f)
        with open(isbn_path, 'r') as f:
            data = json.load(f)
            try:
                titlevalue += data['items'][0]['volumeInfo']['title']
                try:
                    descriptionvalue += data['items'][0]['volumeInfo']['description']
                except:
                    # flash('概要を参照できませんでした', 'alert alert-danger')
                    pass
                try:
                    publishervalue += data['items'][0]['volumeInfo']['authors'][0]
                except:
                    # flash('発行者を参照できませんでした', 'alert alert-danger')
                    pass
                try:
                    publisheddatevalue += data['items'][0]['volumeInfo']['publishedDate']
                except:
                    # flash('発行日を参照できませんでした', 'alert alert-danger')
                    pass
                try:
                    if data['items'][0]['volumeInfo']['industryIdentifiers'][0]['identifier'] == isbn:
                        identifier10value += data['items'][0]['volumeInfo']['industryIdentifiers'][1]['identifier']
                    else:
                        identifier10value += data['items'][0]['volumeInfo']['industryIdentifiers'][0]['identifier']
                except:
                    # flash('ISBN-10を参照できませんでした', 'alert alert-danger')
                    pass
                try:
                    if data['items'][0]['volumeInfo']['industryIdentifiers'][1]['identifier'] == isbn:
                        identifier13value += data['items'][0]['volumeInfo']['industryIdentifiers'][1]['identifier']
                    else:
                        identifier13value += data['items'][0]['volumeInfo']['industryIdentifiers'][0]['identifier']
                except:
                    # flash('ISBN-13を参照できませんでした', 'alert alert-danger')
                    pass
                try:
                    thumbnailvalue += data['items'][0]['volumeInfo']['imageLinks']['thumbnail']
                except:
                    # flash('サムネイルを参照できませんでした', 'alert alert-danger')
                    thumbnailvalue += ""
                    # pass
                try:
                    previewlinkvalue += data['items'][0]['volumeInfo']['previewLink']
                except:
                    # flash('プレビューを参照できませんでした', 'alert alert-danger')
                    pass
                flash('書籍情報を参照しました', 'alert alert-info')
            except:
                flash('書籍情報を参照できませんでした', 'alert alert-danger')


    return render_template('entries/new.html',
        titlevalue=titlevalue,
        descriptionvalue=descriptionvalue,
        publishervalue=publishervalue,
        publisheddatevalue=publisheddatevalue,
        identifier10value=identifier10value,
        identifier13value=identifier13value,
        thumbnailvalue=thumbnailvalue,
        previewlinkvalue=previewlinkvalue)

@entry.route('/entries/new', methods=['GET'])
@login_required
def new_entry():
    return render_template('entries/new.html')

@entry.route('/entries/<int:id>', methods=['GET'])
@login_required
def show_entry(id):
    entry = Entry.query.get(id)
    return render_template('entries/show.html', entry=entry)


@entry.route('/entries/<int:id>/edit', methods=['GET'])
@login_required
def edit_entry(id):
    entry = Entry.query.get(id)
    return render_template('entries/edit.html', entry=entry)


@entry.route('/entries/<int:id>/update', methods=['POST'])
@login_required
def update_entry(id):
    entry = Entry.query.get(id)
    entry.title = request.form['title']
    entry.description = request.form['description']
    entry.publisher=request.form['publisher']
    entry.publisheddate=request.form['publisheddate']
    entry.identifier10=request.form['identifier10']
    entry.identifier13 = request.form['identifier13']
    entry.thumbnail=request.form['thumbnail']
    entry.previewlink=request.form['previewlink']
    entry.tanto=request.form['tanto']
    entry.hokan=request.form['hokan']
    entry.memo=request.form['memo']

    db.session.merge(entry)
    db.session.commit()
    flash('書籍情報が更新されました', 'alert alert-info')
    return redirect(url_for('entry.show_entries'))


# -----


@entry.route('/entries/<int:id>/update_isbn', methods=['POST'])
@login_required
def update_isbn(id):
    isbn = request.form['updateisbn']
    url = 'https://www.googleapis.com/books/v1/volumes?q=isbn:'
    isbn_path = "./isbn_data/" + isbn + ".json"
    req_url = url + isbn
    response = requests.get(req_url)
    with open(isbn_path, 'w') as f:
        json.dump(response.json(), f)
    with open(isbn_path, 'r') as f:
        data = json.load(f)
        try:
            titlevalue = data['items'][0]['volumeInfo']['title']
            try:
                descriptionvalue = data['items'][0]['volumeInfo']['description']
            except:
                descriptionvalue = ""
                pass
            try:
                publishervalue = data['items'][0]['volumeInfo']['authors'][0]
            except:
                publishervalue = ""
                pass
            try:
                publisheddatevalue = data['items'][0]['volumeInfo']['publishedDate']
            except:
                publisheddatevalue = ""
                pass
            try:
                if data['items'][0]['volumeInfo']['industryIdentifiers'][0]['identifier'] == isbn:
                    identifier10value = data['items'][0]['volumeInfo']['industryIdentifiers'][1]['identifier']
                else:
                    identifier10value = data['items'][0]['volumeInfo']['industryIdentifiers'][0]['identifier']
            except:
                pass
            try:
                if data['items'][0]['volumeInfo']['industryIdentifiers'][1]['identifier'] == isbn:
                    identifier13value = data['items'][0]['volumeInfo']['industryIdentifiers'][1]['identifier']
                else:
                    identifier13value = data['items'][0]['volumeInfo']['industryIdentifiers'][0]['identifier']
            except:
                pass
            try:
                thumbnailvalue = data['items'][0]['volumeInfo']['imageLinks']['thumbnail']
            except:
                thumbnailvalue = ""
                pass
            try:
                previewlinkvalue = data['items'][0]['volumeInfo']['previewLink']
            except:
                previewlinkvalue = ""
                pass
            flash('書籍情報を参照しました', 'alert alert-info')
        except:
            flash('書籍情報を参照できませんでした', 'alert alert-danger')



    entry = Entry.query.get(id)
    return render_template('entries/edit.html',
        entry=entry,
        titlevalue=titlevalue,
        descriptionvalue=descriptionvalue,
        publishervalue=publishervalue,
        publisheddatevalue=publisheddatevalue,
        identifier10value=identifier10value,
        identifier13value=identifier13value,
        thumbnailvalue=thumbnailvalue,
        previewlinkvalue=previewlinkvalue)


@entry.route('/entries/<int:id>/delete', methods=['POST'])
@login_required
def delete_entry(id):
    entry = Entry.query.get(id)
    db.session.delete(entry)
    db.session.commit()
    flash('書籍情報が削除されました', 'alert alert-info')
    return redirect(url_for('entry.show_entries'))
