# Python version
Python 3.7

# Installation
git clone https://gitlab.com/masataka.ueino/books.git  
cd books  
python -m venv venv  
venv\Script\activate  
python -m pip install --upgrade pip  
pip install -r requirements.txt  
vim flask_book/config.py (Change secret-key, username and password.)  
python manage.py init_db  
mkdir isbn_data  

# Launch server
python server.py

# Access
http://127.0.0.1:5000
